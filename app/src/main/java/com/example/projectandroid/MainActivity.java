package com.example.projectandroid;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.example.projectandroid.utils.Tools;
import com.google.android.material.textfield.TextInputEditText;

public class MainActivity extends AppCompatActivity {
    private TextInputEditText edtEmail,edtpassword;
    private MyButton btnLogin;
    Window window;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //change status bar
       Tools.setStatusBar(this, android.R.color.white);
        //
        edtEmail = findViewById(R.id.edtUsername);
        edtpassword = findViewById(R.id.edtPassword);
        btnLogin = findViewById(R.id.btnLogin);
        setMyButtonEnable();
        edtEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                setMyButtonEnable();

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edtEmail.getText().toString().equals("hendra") && edtpassword.getText().toString().equals("123")){
                    Intent i = new Intent(MainActivity.this,DashboardActivity.class);
                    startActivity(i);
                    finish();
                }else{
                    Toast.makeText(MainActivity.this,"Email salah",Toast.LENGTH_SHORT).show();
                }

            }
        });
    }
    private void setMyButtonEnable(){
        Editable result = edtEmail.getText();
        if (result!= null && !result.toString().isEmpty()){
            btnLogin.setEnabled(true);
        }else {
            btnLogin.setEnabled(false);
        }

    }
}
