package com.example.projectandroid;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.AppCompatButton;
import androidx.core.content.ContextCompat;

import static android.view.Gravity.CENTER;

public class MyButton extends AppCompatButton {
    private int textcolor;
    private Drawable enabledBackground;
    private Drawable disabledBackground;
    public MyButton(@NonNull Context context) {
        super(context);
        init();
    }

    public MyButton(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MyButton(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        setBackground(isEnabled() ? enabledBackground:disabledBackground);
        setTextColor(textcolor);
        setTextSize(15.f);
        setGravity(CENTER);
        setText(isEnabled() ? "Login" : "Login");
    }

    private void init(){
        textcolor = ContextCompat.getColor(getContext(),android.R.color.background_light);
        enabledBackground = getResources().getDrawable(R.drawable.btn_login);
        disabledBackground = getResources().getDrawable(R.drawable.btn_login_disable);


    }
}
